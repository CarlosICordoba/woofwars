﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class Controlador : MonoBehaviour
{
    [Header("TimerSettings")]
    public bool HaveTime;
    float timer;            //Variables del timer
    public Text Contador;
    public AudioSource song;
    public Image clock;
    public float maxTime = 120;
    public float currentTime = 0;

    [Header("PlayerSettings")]

    public Text ScoreUIP1, ScoreUIP2;
    public Text GamerTagP1, GamerTagp2;
    public GameObject P1, P2;
    Animator P1A, P2A;
    Vector2 StartPosP1, StartPosP2;
    public int ScoreP1=0;          //Variables del jugador
    public int ScoreP2 = 0;
    public Text name1;
    public Text name2;

    [Header("UISettings")]
    public Text Ganador;    //UI
    public Text Empate;
    public Text Ad;
    public GameObject restart;

    [Header("GameLoopSettings")]
    public bool CanDraw;
    public bool CanDig;
    public float TimeForDraw;       //Variables para el gameloop
    public float TimeForDig;
    public float WoofTimer;
    public bool HaveWoof;
    public GameObject[] TileMap;
    public int NumberOfRounds;
    public Sprite Pasto;
    public GameObject MisteryBone;


    

    void Start()
    {
        StartPosP1 = P1.transform.position;
        StartPosP2 = P2.transform.position; //Guardamos la posicion inicial de los jugadores

        P1A=P1.GetComponent<Animator>();
        P2A = P2.GetComponent<Animator>();

        //StartCoroutine("GameLoop");
        StartCoroutine(WoofLoop());

        if (timer!=0) 
        {
            HaveTime = true; //Si el tiempo no es igual a 0 HaveTime es verdadero
        }

        name1.text = PlayerPrefs.GetString("Tag1");
        name2.text = PlayerPrefs.GetString("Tag2");
        GamerTagP1.text = name1.text + ":";
        GamerTagp2.text = name2.text + ":";

        currentTime = maxTime;
    }

    void Update()
    {
        Timer();

        if(ScoreP1<0)
        {
            ScoreP1 = 0;
            ScoreUIP1.text = ScoreP1.ToString();
        }
        
        if(ScoreP2<0)
        {
            ScoreP2 = 0;
            ScoreUIP2.text = ScoreP2.ToString();
        }

        //Debug.Log(name1.text);

        

    }

    //Aqui se va restando el tiempo para el fill amount
    void DecreaseTime()
    {
        currentTime -= 1f;
        float calculateTime = currentTime / maxTime;
        SetTime(calculateTime);
    }

    //Aqui va desapareciendo el fillamount conforme al tiempo
    void SetTime(float myTime)
    {
        clock.fillAmount = myTime;
    }

    IEnumerator GameLoop()
    {
        while(NumberOfRounds>0)
        {

            Ad.text = "3";
            yield return new WaitForSeconds(1);
            Ad.text = "2";
            yield return new WaitForSeconds(1);
            Ad.text = "1";
            yield return new WaitForSeconds(1);
            Ad.text = "¡¡A pintar!!";
            yield return new WaitForSeconds(.5f);
            Ad.text = "";

            Debug.Log("Fase1"); 
            DrawPhase(); //Fase de pintura

            yield return new WaitForSeconds(timer);
            
            ResetPlayersPosition();
            SetBones();


            Ad.text = "3";
            yield return new WaitForSeconds(1);
            Ad.text = "2";
            yield return new WaitForSeconds(1);
            Ad.text = "1";
            yield return new WaitForSeconds(1);
            Ad.text = "¡¡A excavar!!";
            yield return new WaitForSeconds(.5f);
            Ad.text = "";

            Debug.Log("Fase2");
            DigPhase();

            yield return new WaitForSeconds(timer);

            ResetPlayersPosition();
            ResetTiles();
            NumberOfRounds -= 1;        

        }

        if(NumberOfRounds<=0)
        {
            Debug.Log("Termino el juego");
            ChooseWinner();
            StopCoroutine("GameLoop");
        }

    }

    IEnumerator WoofLoop()
    {
        while(NumberOfRounds>0)
        {
            SetBones();

            Ad.text = "3";
            yield return new WaitForSeconds(1);
            Ad.text = "2";
            yield return new WaitForSeconds(1);
            Ad.text = "1";
            yield return new WaitForSeconds(1);
            Ad.text = "¡¡WoofWars!!";
            yield return new WaitForSeconds(.5f);
            Ad.text = "";
            HaveWoof = true;
            WoofWars();
            yield return new WaitForSeconds(timer);

            HaveWoof = false;
            ResetPlayersPosition();
            ResetTiles();
            NumberOfRounds -= 1;

        }
        
        if (NumberOfRounds <= 0)
        {
            Debug.Log("Termino el juego");
            ChooseWinner();
            StopCoroutine(WoofLoop());
            StopCoroutine(WoofPower());
        }

    }

    IEnumerator WoofPower()
    {
        bool IsPowerActive=false;
        yield return new WaitForSeconds(20f);

        while(HaveWoof==true)
        {
            Vector3 CenterPosition;
            float Distance;
            CenterPosition = Vector3.Lerp(P1.transform.position, P2.transform.position, 0.5f);
            Distance = Vector3.Distance(P1.transform.position, P2.transform.position);

            if (Distance > 5.5f)
            {
                if (!IsPowerActive)
                {
                    Instantiate(MisteryBone, CenterPosition, Quaternion.identity);
                    IsPowerActive = true;
                    yield return new WaitForSeconds(10);
                    IsPowerActive = false;
                }
            }
            else if(Distance < 5.5f)
            {
                yield return new WaitForSeconds(.5f);
            }

        }

    }


   public void UpdateScore(int Player, bool Match, int Points) 
    {
        //Aqui se actualiza el score de ambos jugadores
        if (Player==1 && Match == false)
        {
            ScoreP1=ScoreP1 + Points;
            ScoreUIP1.text = ScoreP1.ToString();
        }
        
        if(Player==2 && Match == false)
        {
            ScoreP2= ScoreP2 + Points;
            ScoreUIP2.text = ScoreP2.ToString();
        }

        if(Player == 1 && Match == true && ScoreP2!=0)
        {
            ScoreP1= ScoreP1 + Points;
            ScoreP2= ScoreP2 - Points;
            ScoreUIP1.text = ScoreP1.ToString();
            ScoreUIP2.text = ScoreP2.ToString();
        }

        if (Player == 2 && Match == true && ScoreP1 != 0)
        {
            ScoreP1 = ScoreP1 - Points;
            ScoreP2 = ScoreP2 + Points;
            ScoreUIP1.text = ScoreP1.ToString();
            ScoreUIP2.text = ScoreP2.ToString();
        }


    }

    void Timer()
    {
        if (timer <= 0)
        {
            //Cuando ya no haya tiempo HaveTime es falso
            HaveTime = false;
            
        }
        else if (timer >= 0)
        {
            
            timer -= Time.deltaTime;
            Contador.text = timer.ToString("f0"); //Resta el tiempo
        } //Cuando el tiempo se acabe los jugadores no se podran mover
    }

    void DrawPhase()
    {
        CanDraw = true;
        CanDig = false;
        if(CanDraw==true)
        {
            timer = TimeForDraw;
            HaveTime = true;
        }
        
    }

    void DigPhase()
    {
        CanDig = true;
        CanDraw = false;
        if (CanDig == true)
        {
            timer = TimeForDig;
            HaveTime = true;
        }
    }

    void WoofWars()
    {
        StartCoroutine(WoofPower());
        CanDraw = true;
        CanDig = true;
        if(CanDig==true&&CanDraw==true)
        {
            InvokeRepeating("DecreaseTime", .5f, 1f);         
            timer = WoofTimer;
            HaveTime = true;
        }
    }

    void ChooseWinner()
    {
        if (ScoreP1 > ScoreP2)
        {
            Ganador.gameObject.SetActive(true);
            Ganador.text = "¡¡Gano " + name1.text + "!!";
        }
        else if (ScoreP2>ScoreP1)
        {
            Ganador.gameObject.SetActive(true);
            Ganador.text = "¡¡Gano " + name2.text + "!!";
        }
        else if(ScoreP1==ScoreP2)
        {
            Empate.gameObject.SetActive(true);
        }
       // restart.SetActive(true);
        //song.Stop();
        

    }

    void ResetPlayersPosition()
    { 
         //Reiniciamos la posicion de los jugadores a la inicial
        P2.transform.position = StartPosP2;
        P1.transform.position = StartPosP1;
    }

    void SetBones()
    {
        Pintar Tile;  
        int Lucky;
        for(int i=0;i<TileMap.Length;i++)
        {
            Tile= TileMap[i].GetComponent<Pintar>();
            Lucky = Random.Range(1, 10);
            if(Lucky<=2) //20% de que haya un hueso
            {
                Debug.Log("Bone");
                Tile.HaveBone = true;
            }
            else
            {
                Tile.HaveBone = false;

            }
        }
    }

    void ResetTiles()
    {
        Pintar Bone;
        SpriteRenderer Tile;

        for (int i = 0; i < TileMap.Length; i++)
        {
            Bone = TileMap[i].GetComponent<Pintar>();
            Bone.HaveBone = false;
            Tile = TileMap[i].GetComponent<SpriteRenderer>();
            StartCoroutine(Delay(Tile, Color.white));
            Tile.sprite = Pasto;
            Debug.Log("Blanco");
        }
    }


    IEnumerator Delay(SpriteRenderer Sprite,Color color) //Rastro de pintura
    {
        float seg = 3f;
        while (seg > 0)
        {
            seg -= Time.deltaTime;
            Sprite.color = Color.Lerp(color, Sprite.color, seg - 0.1f);

            yield return 0;
        }

    }

}
