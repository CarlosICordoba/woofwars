﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pintar : MonoBehaviour
{
    [Header("PlayerSprites")]
    public Sprite player1;
    public Sprite player2;
    public GameObject aviso;

    Movimiento Control;
    Movimiento2 Control1;

    Huesos Bones1;
    Huesos2 Bones2;

    public float TimeSpawnTile = 5;
    public float TimeSpawnBone = 3;
    public bool HaveBone;
    public bool Señal;
    public bool Dirt = false;
    public SpriteRenderer m_Srenderer;
    public Sprite Tierra;
    Controlador Controlador;
    SpriteRenderer p_renderer;

    public AudioClip excavar;
    AudioSource m_Source1;
    AudioSource m_Source2;

    [HideInInspector] public bool Polvo = false;
    public ParticleSystem particleObject;
    public GameObject particleGameObject;

    [HideInInspector] public bool Polvo2 = false;
    public ParticleSystem particleObject2;
    public GameObject particleGameObject2;

    private void Start()
    {
        GameObject ControladorGameObject = GameObject.FindGameObjectWithTag("GameController");
        Controlador = ControladorGameObject.gameObject.GetComponent<Controlador>();
        
        p_renderer = GetComponent<SpriteRenderer>();
        

        GameObject Player1 = GameObject.FindGameObjectWithTag("Player");
        Control = Player1.gameObject.GetComponent<Movimiento>();

        GameObject Player2 = GameObject.FindGameObjectWithTag("Player2");
        Control1 = Player2.gameObject.GetComponent<Movimiento2>();

        GameObject Huseitos = GameObject.FindGameObjectWithTag("Bone");
        Bones1 = Huseitos.gameObject.GetComponent<Huesos>();

        GameObject Huseitos2 = GameObject.FindGameObjectWithTag("Bone2");
        Bones2 = Huseitos2.gameObject.GetComponent<Huesos2>();


    }

    private void Update()
    {
       if(HaveBone==false)
        {
            Señal = false;
            aviso.SetActive(false);
        }


        if(Dirt==true)
        {
            p_renderer.color = Color.white;
        }

        if(Señal==true&&HaveBone==true)
        {
            aviso.SetActive(true);
        }
        else if(Señal==false)
        {
            aviso.SetActive(false);
        }


    }


    IEnumerator Delay(Color color) //Rastro de pintura
    {
            float seg = 1f;
            while (seg > 0)
            {
                seg -= Time.deltaTime;
                m_Srenderer.color = Color.Lerp(Color.white,color , seg - 0.1f);
                yield return 0;
            }      
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {

        if(collision.gameObject.tag== "Player" || collision.gameObject.tag == "Player2")
        {
            if(HaveBone==true)
            {
                Debug.Log("Avisar");
                StartCoroutine(Advice());
            }

        }

    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (Controlador.CanDraw==true)
        {
            if (collision.gameObject.tag == "Player"  && m_Srenderer.sprite == player2 && Dirt==false)
            {
                
                StartCoroutine(Delay(new Color(255, 255, 255, 0.5f)));
                m_Srenderer.sprite = player1;
                Controlador.UpdateScore(1, true, 1); //Aqui actualiza el score de ambos jugadores
                Control.PowerUpCharger(1);
                StartCoroutine(SpawnBone());
            }
            else if (collision.gameObject.tag == "Player"  && m_Srenderer.sprite != player1 && Dirt == false)
            {
                StartCoroutine(Delay(new Color(255, 255, 255, 0.5f)));
                m_Srenderer.sprite = player1;
                Controlador.UpdateScore(1, false, 1); //Aqui actualiza el score del jugador 1
                Control.PowerUpCharger(1);
                StartCoroutine(SpawnBone());
            }

            if (collision.gameObject.tag == "Player2"  && m_Srenderer.sprite== player1 && Dirt == false)
            {
                StartCoroutine(Delay(new Color(255, 255, 255, 0.5f)));
                m_Srenderer.sprite= player2;
                Controlador.UpdateScore(2, true, 1); //Aqui actualiza el score de ambos jugadores
                Control1.PowerUpCharger(1);
                StartCoroutine(SpawnBone());
            }
            else if (collision.gameObject.tag == "Player2"  && m_Srenderer.sprite != player2 && Dirt == false)
            {
                StartCoroutine(Delay(new Color(255, 255, 255, 0.5f)));
                m_Srenderer.sprite = player2;
                Controlador.UpdateScore(2, false, 1); //Aqui actualiza el score del jugador 2
                Control1.PowerUpCharger(1);
                StartCoroutine(SpawnBone());
            }

            if (collision.gameObject.tag == "Bone" && m_Srenderer.sprite == player2 && Dirt == false)
            {
                StartCoroutine(Delay(new Color(255, 255, 255, 0.5f)));
                m_Srenderer.sprite = player1;
                Controlador.UpdateScore(1, true, 1); //Aqui actualiza el score de ambos jugadores
                StartCoroutine(SpawnBone());
            }
            else if (collision.gameObject.tag == "Bone" && m_Srenderer.sprite != player1 && Dirt == false)
            {
                StartCoroutine(Delay(new Color(255, 255, 255, 0.5f)));
                m_Srenderer.sprite = player1;
                Controlador.UpdateScore(1, false, 1); //Aqui actualiza el score del jugador 1
                StartCoroutine(SpawnBone());
            }

            if (collision.gameObject.tag == "Bone2" && m_Srenderer.sprite == player1 && Dirt == false)
            {
                StartCoroutine(Delay(new Color(255, 255, 255, 0.5f)));
                m_Srenderer.sprite = player2;
                Controlador.UpdateScore(2, true, 1); //Aqui actualiza el score de ambos jugadores
                StartCoroutine(SpawnBone());
            }
            else if (collision.gameObject.tag == "Bone2" && m_Srenderer.sprite!= player2 && Dirt == false)
            {
                StartCoroutine(Delay(new Color(255, 255, 255, 0.5f)));
                m_Srenderer.sprite = player2;
                Controlador.UpdateScore(2, false, 1); //Aqui actualiza el score del jugador 2
                StartCoroutine(SpawnBone());
            }

        }      
    }

    private void OnTriggerStay2D(Collider2D col)
    {
        
        if(Controlador.CanDig==true && HaveBone==true)
        {
            if (col.gameObject.tag == "Player")
            {
               // Debug.Log("Tsss");
                if (Sinput.GetButtonDown("Excavar", Control.slot))
                {
                    m_Source1 = col.GetComponent<AudioSource>();
                    Dirt = true;                    
                    m_Srenderer.sprite = Tierra;
                    Bones1.LoadBone();
                    Control.PowerUpCharger(1);
                    HaveBone = false;
                    StartCoroutine(ReloadTile());
                    m_Source1.clip = excavar;
                    m_Source1.volume = 0.2f;
                    m_Source1.Play();

                    Polvo = true;

                    particleGameObject.transform.position = col.transform.position;

                    particleObject.Play();
                }
            }

            if (col.gameObject.tag == "Player2")
            {
                //Debug.Log("Tsss");
                if (Sinput.GetButtonDown("Excavar",Control1.slot ))
                {
                    m_Source2 = col.GetComponent<AudioSource>();
                    Dirt = true;
                    m_Srenderer.sprite = Tierra;
                    Bones2.LoadBone();
                    Control1.PowerUpCharger(1);
                    HaveBone = false;                    
                    StartCoroutine(ReloadTile());
                    m_Source2.clip = excavar;
                    m_Source2.volume = 0.2f;
                    m_Source2.Play();

                    Polvo2 = true;

                    particleGameObject2.transform.position = col.transform.position;

                    particleObject2.Play();
                }
            }          
        }
    }

    IEnumerator ReloadTile()
    {
        
        int Lucky;
        Lucky = Random.Range(1, 10);

        yield return new WaitForSeconds(TimeSpawnTile);
      
        m_Srenderer.sprite = Controlador.Pasto;
        m_Srenderer.color = Color.white;

        if (Lucky <= 2) //20% de que haya un hueso
        {
            Debug.Log("Bone");
            HaveBone = true;
        }
        else
        {
            HaveBone = false;

        }
        Dirt = false;

    }

    IEnumerator SpawnBone()
    {
        int Lucky;
        Lucky = Random.Range(1, 10);

        yield return new WaitForSeconds(TimeSpawnBone);

        if (Lucky <= 1) //10% de que haya un hueso
        {
            Debug.Log("Bone");
            HaveBone = true;
            
        }
        else
        {
            HaveBone = false;

        }
    }

    IEnumerator Advice()
    {
        if(HaveBone==true)
        {
            int Lucky;
            Lucky = Random.Range(1, 10);

            yield return new WaitForSeconds(1.5f);

            if (Lucky <= 5) //50% de que te avise
            {
                Señal = true;
            }
            else
            {
                Señal = false;
            }

        }

    }

}
