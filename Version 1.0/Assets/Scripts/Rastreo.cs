﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rastreo : MonoBehaviour
{
    public SinputSystems.InputDeviceSlot slot;

    public GameObject Humo;
    bool Rastrear;

    

    private void OnTriggerEnter2D(Collider2D collision)
    {
        Pintar Tile;
        if(collision.gameObject.tag=="Floor")
        {
            Tile=collision.GetComponent<Pintar>();
            if(Tile.HaveBone == true)
            {
                Instantiate(Humo, new Vector3(Tile.transform.position.x,Tile.transform.position.y) , Tile.gameObject.transform.rotation);
            }
            
        }
    }

    
}
