﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cmapa : MonoBehaviour
{
    SpriteRenderer sr;
    int count;
    // Start is called before the first frame update
    void Start()
    {
        sr = GetComponent<SpriteRenderer>();

    }

    // Update is called once per frame
    void Update()
    {
        if (sr.color == Color.black)
        {
            if (count > 750)
            {
                sr.color = Color.white;
                count = 0;
            }
        }
        count++;
    }
}
