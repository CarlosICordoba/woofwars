﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.EventSystems;


public class PauseMenu : MonoBehaviour
{
    public static bool gameIsPaused = false;
    Controlador time;
    public GameObject pauseMenuUI;
    public Button resume;
    public Button restart;
   

    public GameObject restarMenu;
    public AudioSource stop;
    public EventSystem eventSystem;
    public EventSystem eventSysten2;

    // Update is called once per frame
    private void Start()
    {
        gameIsPaused = false;
        GameObject ControladorObject = GameObject.FindGameObjectWithTag("GameController");
        time = ControladorObject.gameObject.GetComponent<Controlador>();
    }
    void Update()
    {
        if (Sinput.GetButtonDown("Pausa"))
        {
            if (gameIsPaused)
            {
                Resume();
            }
            else
            {
                Pause();
            }

        }


        if (pauseMenuUI.activeSelf)
        {
            eventSystem.gameObject.SetActive(true);
            eventSysten2.gameObject.SetActive(false);
        }
        else if (restarMenu.activeSelf)
        {
            eventSystem.gameObject.SetActive(false);
            eventSysten2.gameObject.SetActive(true);

        }
        else
        {
            eventSystem.gameObject.SetActive(false);
            eventSysten2.gameObject.SetActive(false);

        }





    }
    public void Resume()
    {
        if(time.HaveTime==true)
        {
            pauseMenuUI.SetActive(false);
        }
        else if(time.HaveTime==false && time.HaveWoof==false)
        {
            restarMenu.SetActive(false);
        }
        Time.timeScale = 1;
        stop.UnPause();
        gameIsPaused = false;
    }
    void Pause()
    {
        if(time.HaveWoof)
        {
            if (time.HaveTime == true)
            {
                pauseMenuUI.SetActive(true);
            }

            Time.timeScale = 0;
            stop.Pause();
            gameIsPaused = true;
        }
        else if(!time.HaveWoof && time.NumberOfRounds<=0)
        {
            if (time.HaveTime == false)
            {
                restarMenu.SetActive(true);
            }

            Time.timeScale = 0;
            stop.Pause();
            gameIsPaused = true;
        }

    }
   
    public void LoadMenu()
    {
        Time.timeScale = 1;
        SceneManager.LoadScene(0);
    }
    public void Quit()
    {
        Application.Quit();
    }
     public void ReLoad()
     {
        SceneManager.LoadScene(1);
        Resume();
    }
}
