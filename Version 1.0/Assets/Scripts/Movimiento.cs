﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;



public class Movimiento : MonoBehaviour
{
    [Header("PlayerVar")]
    public SinputSystems.InputDeviceSlot slot;

  
    
    Controlador Controlador;
    public float vel;
    Transform player;
    Animator m_Animator;
    public Transform BarkCollider;
    public int[] Directions = { 90, 0, -90, 180 };
    int Direction = 1; //0:UP 1:LEFT 2:DOWN 3:RIGHT

    [Header("Audio")]
    AudioSource m_Source;
    public AudioClip grande;
    public AudioClip velocidad;
    public AudioClip slow;
    public AudioClip boneDamage;

    public GameObject powerUpBar;

    [Header("PowerUpSprites")]
    SpriteRenderer renderHolder;
    public SpriteRenderer rendererSprite;
    public Sprite grandeS;
    public Sprite speedS;
    public Sprite slowS;
    public Sprite stunS;
    public Sprite holderS;
    [Header("StunVar")]
    public float StunDuration;
    public bool CanMove = false;
    [Header("SlowVar")]
    public float SlowDuration;
    public float DebuffVel;
    public bool IsSlow = false;
    [Header("SpeedVar")]    
    public float BuffVel;
    public float SpeedBuffDuration;
    public bool MoreSpeed = false;
    [Header("SizeVar")]
    public float StatsSize;
    public float SizeDuration;
    public bool Size = false;
    [Header("PowerPoints")]
    public int PointToHavePower;
    public GameObject UI;
    int PointToPower;
    public Image bar;
    public float maxTime = 90;
    public float currentTime = 0;

    void Start()
    {
        GameObject ControladorGameObject = GameObject.FindGameObjectWithTag("GameController");
        m_Animator = GetComponent<Animator>();
        m_Source = GetComponent<AudioSource>();

        Controlador = ControladorGameObject.gameObject.GetComponent<Controlador>(); 
        player = GetComponent<Transform>();

        renderHolder = GameObject.Find("PowerUp Holder").GetComponent<SpriteRenderer>();
        renderHolder.sprite = holderS;

        //rendererSprite = GameObject.Find("PowerUp").GetComponent<SpriteRenderer>();
        //rendererSprite.sprite = null;
        
        
        slot = ButtonController.slot1;


    }
    
    // Update is called once per frame
    void Update()
    {
        if(CanMove==false)
        {
            StartCoroutine(StunDebuff());
        }

        if(IsSlow==true&&vel>=0.05f)
        {
            StartCoroutine(SlowDebuff(slow));
        }

        if(MoreSpeed==true)
        {
            StartCoroutine(SpeedBuff(velocidad));
        }

        if(Size==true)
        {
            StartCoroutine(SizeBuff(grande));   
        }
       
      
        if (Controlador.HaveTime==true&&CanMove==true) //Mientras haya tiempo el jugador se podra mover
        {
            if(!PauseMenu.gameIsPaused)
            {
                if (PointToPower >= PointToHavePower)
                {
                    powerUpBar.SetActive(true);
                }
                else
                {
                    powerUpBar.SetActive(false);
                }


                if (PointToPower >= PointToHavePower && Sinput.GetButtonDown("PowerUp", slot))
                {
                    int Lucky;
                    PointToPower = PointToPower - PointToHavePower;
                    currentTime = 0f;
                    Lucky = Random.Range(1, 10);
                    Debug.Log(PointToPower);

                    if (Controlador.ScoreP1 <= Controlador.ScoreP2)
                    {
                        if (Lucky<=6)
                        {
                            Debug.Log("Velocidad");
                            MoreSpeed = true;
                        }
                        else
                        {
                            Debug.Log("ENORME");
                            Size = true;
                        }
                    }
                    else if (Controlador.ScoreP1>=Controlador.ScoreP2)
                    {
                        if(Lucky<=7)
                        {
                            Debug.Log("ENORME");
                            Size = true;
                        }
                        else if(Lucky>7)
                        {
                            Debug.Log("Velocidad");
                            MoreSpeed = true;
                        }
                    }
                }

                if (Sinput.GetButton("Up",slot))
                {
                    m_Animator.SetBool("Stop", false);
                    player.position = new Vector2(player.position.x, player.position.y + vel);
                    m_Animator.SetTrigger("Up");
                    Direction = 0;
                }
                else if (Sinput.GetButton("Left", slot))
                {
                    m_Animator.SetBool("Stop", false);
                    player.position = new Vector2(player.position.x - vel, player.position.y);
                    m_Animator.SetTrigger("Left");
                    Direction = 3;
                }
                else if (Sinput.GetButton("Right", slot))
                {
                    m_Animator.SetBool("Stop", false);
                    player.position = new Vector2(player.position.x + vel, player.position.y);
                    m_Animator.SetTrigger("Right");
                    Direction = 1;
                }
                else if (Sinput.GetButton("Down", slot))
                {
                    m_Animator.SetBool("Stop", false);
                    player.position = new Vector2(player.position.x, player.position.y - vel);
                    m_Animator.SetTrigger("Down");
                    Direction = 2;
                }
                else
                {
                    m_Animator.SetBool("Stop", true);
                    Direction = 1;
                }
                BarkCollider.rotation = Quaternion.Euler(0, 0, Directions[Direction]);
            }
        
            //Change the position depending on the vector
        }
        else
        {
            m_Animator.SetBool("Stop", true);

        } 

             
    }

    //Aqui se va restando el tiempo para el fill amount
    void DecreaseTime()
    {
        currentTime += 1f;
        float calculateTime = currentTime / maxTime;
        SetTime(calculateTime);
    }

    //Aqui va desapareciendo el fillamount conforme al tiempo
    void SetTime(float myTime)
    {
        bar.fillAmount = myTime;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.tag=="Bone2")
        {
            CameraShake.ShakingCam = true;
            CameraShake.CaseShakingCam = 1;
            CanMove = false;
            Destroy(collision.gameObject);
            m_Source.clip = boneDamage;
            m_Source.volume = 0.8f;
            m_Source.Play();
        }
    }

    IEnumerator StunDebuff()
    {
        if(CanMove==false)
        {
            rendererSprite.sprite = stunS;
            yield return new WaitForSeconds(StunDuration);
            CanMove = true;
            rendererSprite.sprite = null;
        }
    }

    IEnumerator SlowDebuff(AudioClip clip)
    {
        if (IsSlow == true)
        {
            IsSlow = false;
            rendererSprite.sprite = slowS;
            m_Source.clip = clip;
            m_Source.volume = 0.8f;
            m_Source.Play();
            vel = vel - DebuffVel;
            yield return new WaitForSeconds(SlowDuration);           
            vel = vel + DebuffVel;
            rendererSprite.sprite = null;
        }
    }

    IEnumerator SpeedBuff(AudioClip clip)
    {
        if(MoreSpeed==true)
        {
            MoreSpeed = false;
            rendererSprite.sprite = speedS;
            m_Source.clip = clip;
            m_Source.volume = 0.15f;
            m_Source.Play();
            vel = vel + BuffVel;
            yield return new WaitForSeconds(SpeedBuffDuration);
            vel = vel - BuffVel;
            rendererSprite.sprite = null;
        }
    }

    IEnumerator SizeBuff(AudioClip clip)
    {
        if(Size==true)
        {
            Size = false;
            rendererSprite.sprite = grandeS;
            m_Source.clip = clip;
            m_Source.volume = 1;
            m_Source.Play();
            player.transform.localScale = new Vector2(player.transform.localScale.x + StatsSize, player.transform.localScale.y + StatsSize);
            yield return new WaitForSeconds(SizeDuration);
            player.transform.localScale = new Vector2(player.transform.localScale.x - StatsSize, player.transform.localScale.y - StatsSize);
            rendererSprite.sprite = null;
        }
    }

    public void PowerUpCharger(int points)
    {
        PointToPower = PointToPower + points;
        //Debug.Log(PointToPower);
        Invoke("DecreaseTime", 1f);
    }

}

