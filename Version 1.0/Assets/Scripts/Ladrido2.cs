﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ladrido2 : MonoBehaviour
{
    public int Points;
    bool CanBark;
    Movimiento Player1;
    Movimiento2 Player2;
    Controlador Controlador;
    AudioSource m_Source;
    public AudioClip ladrido;

    private void Start()
    {
        GameObject PlayerControler = GameObject.FindGameObjectWithTag("Player");
        Player1 = PlayerControler.GetComponent<Movimiento>();

        GameObject PlayerControler2 = GameObject.FindGameObjectWithTag("Player2");
        Player2 = PlayerControler2.GetComponent<Movimiento2>();

        GameObject ControladorGameObject = GameObject.FindGameObjectWithTag("GameController");
        Controlador = ControladorGameObject.gameObject.GetComponent<Controlador>();
        m_Source = GetComponentInParent<AudioSource>();

        CanBark = false;
    }

    void Update()
    {
        if (CanBark == true)
        {
            if (Sinput.GetButtonDown("Ladrar", Player2.slot)&&Player1.vel>=0.05f)
            {
                m_Source.clip = ladrido;
                m_Source.volume = 1;
                m_Source.Play();
                Player1.IsSlow = true;
                Player2.PowerUpCharger(Points);
                CameraShake.ShakingCam = true;
                CameraShake.CaseShakingCam = 2;
                Debug.Log("Ladrido");
            }
        }
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            //Debug.Log("PuedoLadrar2");
            CanBark = true;
        }

    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            Debug.Log("NoPuedoLadrar2");
            CanBark = false;
        }
    }

}
