﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraShake : MonoBehaviour
{
    private Vector3 startingPosition;
    private float RandomRangeX;
    private float RandomRangeY;
    private float RandomRangeXR;
    private float RandomRangeYR;
    private float RandomRangeZR;
    public float ShakingTime;
    private Vector3 startingRotation;
    [HideInInspector] public static bool ShakingCam;
    [HideInInspector] public static int CaseShakingCam;
    // Start is called before the first frame update
    void Start()
    {
        ShakingCam = false;
        startingPosition = gameObject.transform.position;
        startingRotation = gameObject.transform.eulerAngles;
    }

    // Update is called once per frame
    void Update()
    {
        if(CaseShakingCam==1)
        {
            RandomRangeX = Random.Range(-.1f, .1f);
            RandomRangeY = Random.Range(-.1f, .1f);
            RandomRangeZR = Random.Range(-.1f, .1f);
        }
        else if(CaseShakingCam==2)
        {
            RandomRangeX = Random.Range(-.05f, .05f);
            RandomRangeY = Random.Range(-.05f, .05f);
            RandomRangeZR = Random.Range(-.05f, .05f);
        }


        if(ShakingCam)
        {
            gameObject.transform.position = new Vector3(RandomRangeX,RandomRangeY,-10);
            gameObject.transform.eulerAngles = new Vector3(0,0,RandomRangeZR);
            Invoke("StopShaking", ShakingTime);
        }
    }

    void StopShaking()
    {
        ShakingCam = false;
        gameObject.transform.position = startingPosition;
        gameObject.transform.eulerAngles = startingRotation;
    }
}
