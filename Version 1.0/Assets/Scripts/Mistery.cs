﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mistery : MonoBehaviour
{
    Movimiento P1;
    Movimiento2 P2;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        int SelectPower;

        if(collision.tag=="Player")
        {
            P1 = collision.GetComponent<Movimiento>();
            SelectPower = Random.Range(1, 3);
            if(SelectPower==1)
            {
                P1.MoreSpeed = true;
            }
            else if(SelectPower==2)
            {
                P1.Size = true;
            }
            else if(SelectPower==3)
            {
                P1.IsSlow = true;
            }

            Destroy(gameObject);
        }
        else if(collision.tag=="Player2")
        {
            P2 = collision.GetComponent<Movimiento2>();
            SelectPower = Random.Range(1, 3);
            if (SelectPower == 1)
            {
                P2.MoreSpeed = true;
            }
            else if (SelectPower == 2)
            {
                P2.Size = true;
            }
            else if (SelectPower == 3)
            {
                P2.IsSlow = true;
            }
            Destroy(gameObject);
        }
    }
}
