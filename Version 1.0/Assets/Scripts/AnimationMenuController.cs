﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AnimationMenuController : MonoBehaviour
{
    Animator player;
    [SerializeField]
    Animator holder;
    public Text playerT;
    
    // Start is called before the first frame update
    private void Start()
    {
        player = GetComponent<Animator>();
        holder = playerT.gameObject.GetComponent<Animator>();
     
    }

    // Update is called once per frame
    void Update()
    {
        if(playerT.text != "Press Start")
        {
            player.SetBool("Move", false);
            holder.SetBool("Move", false);
        }
        else
        {
            player.SetBool("Move", true);
            holder.SetBool("Move", true);
        }
        
    }
}
