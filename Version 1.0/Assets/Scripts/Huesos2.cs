﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Huesos2 : MonoBehaviour
{

    public float Delay = 1.5f;
    bool CanShoot;
    public GameObject[] EmptyBones;
    public Sprite FullBone;
    public Sprite EmptyBone;
    public int NumBones = 1;
    public GameObject[] Bones;
    public Transform Spawn;

    Controlador Controlador;
    Movimiento2 Player;

    void Start()
    {
        GameObject ControladorGameObject = GameObject.FindGameObjectWithTag("GameController");
        Controlador = ControladorGameObject.gameObject.GetComponent<Controlador>();

        GameObject PlayerGameObject = GameObject.FindGameObjectWithTag("Player2");
        Player = PlayerGameObject.gameObject.GetComponent<Movimiento2>();

        for (int i = 0; i < NumBones; i++)
        {
            EmptyBones[i].GetComponent<SpriteRenderer>().sprite = FullBone;
        }

        CanShoot = true;

    }

    void Update()
    {
        if(CanShoot==false)
        {
            StartCoroutine(DelayShoot());
        }
        else if (CanShoot == true)
        {
            StopCoroutine(DelayShoot());
        }

        if (NumBones != 0 && Controlador.HaveTime == true && Player.CanMove == true && CanShoot==true)
        {
            if (Sinput.GetButton("Up", Player.slot) && Sinput.GetButtonDown("Disparar", Player.slot))
            {
                CanShoot = false;
                InstantiateBone(0,90);
                CleanBone();
            }
            else if (Sinput.GetButton("Left", Player.slot) && Sinput.GetButtonDown("Disparar", Player.slot))
            {
                CanShoot = false;
                InstantiateBone(1,0);
                CleanBone();
            }
            else if (Sinput.GetButton("Right", Player.slot) && Sinput.GetButtonDown("Disparar", Player.slot))
            {
                CanShoot = false;
                InstantiateBone(2,0);
                CleanBone();
            }
            else if (Sinput.GetButton("Down", Player.slot) && Sinput.GetButtonDown("Disparar", Player.slot))
            {
                CanShoot = false;
                InstantiateBone(3,90);
                CleanBone();
            }
        }
    }

    public void LoadBone()
    {
        if (NumBones != 3)
        {
            NumBones += 1;
            for (int i = 0; i < NumBones; i++)
            {
                EmptyBones[i].GetComponent<SpriteRenderer>().sprite = FullBone;
            }
        }

    }

    void CleanBone()
    {
        NumBones -= 1;
        CanShoot = false;
        EmptyBones[NumBones].GetComponent<SpriteRenderer>().sprite = EmptyBone;

    }

    //Rotacion
    void InstantiateBone(int direccion, float rotacion)
    {
        Instantiate(Bones[direccion], Spawn.position, Quaternion.Euler(0,0,rotacion));
    }

    IEnumerator DelayShoot()
    {
        if(CanShoot==false)
        {
            yield return new WaitForSeconds(Delay);
            CanShoot = true;
        }
    }
}
