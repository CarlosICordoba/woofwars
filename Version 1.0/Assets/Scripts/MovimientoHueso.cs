﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovimientoHueso : MonoBehaviour
{
    public int Direccion;
    public float speed;
    float timelife = 5;
    Transform Bone;

    private void Awake()
    {
        Bone = GetComponent<Transform>();
    }


    private void Update()
    {
        timelife -= Time.deltaTime;
        if(timelife<=0)
        {
            Destroy(this.gameObject);
        }

        if(Direccion==0)
        {
            Bone.position = new Vector2(Bone.position.x, Bone.position.y + speed);
        }
        else if (Direccion == 1)
        {
            Bone.position = new Vector2(Bone.position.x - speed, Bone.position.y);
        }
        else if(Direccion==2)
        {
            Bone.position = new Vector2(Bone.position.x + speed, Bone.position.y);
        }
        else if(Direccion==3)
        {
            Bone.position = new Vector2(Bone.position.x, Bone.position.y - speed);
        }
    }
}
