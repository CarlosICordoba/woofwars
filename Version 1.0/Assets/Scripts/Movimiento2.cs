﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Movimiento2 : MonoBehaviour
{
    public GameObject powerUpbar;
    [Header("PlayerVar")]
    public SinputSystems.InputDeviceSlot slot;


    [Header("PowerUpSprites")]
    SpriteRenderer renderHolder;
    SpriteRenderer rendererSprite;
    public Sprite grandeS;
    public Sprite speedS;
    public Sprite slowS;
    public Sprite stunS;
    public Sprite holderS;

    Controlador Controlador;
    Animator player2;
    public float vel;
    Transform player;
    public Transform BarkCollider;
    public int[] Directions = { 90, 0, -90, 180 };
    int Direction = 1; //0:UP 1:LEFT 2:DOWN 3:RIGHT

    [Header("Audio")]
    AudioSource m_Source;
    public AudioClip grande;
    public AudioClip velocidad;
    public AudioClip slow;
    public AudioClip boneDamage;

    [Header("StunVar")]
    public float StunDuration;
    public bool CanMove = false;
    [Header("SlowVar")]
    public float SlowDuration;
    public float DebuffVel;
    public bool IsSlow = false;
    [Header("SpeedVar")]
    public float BuffVel;
    public float SpeedBuffDuration;
    public bool MoreSpeed = false;
    [Header("SizeVar")]
    public float StatsSize;
    public float SizeDuration;
    public bool Size = false;
    [Header("PowerPoints")]
    public int PointToHavePower;
    public GameObject UI;
    int PointToPower;
    public Image bar;
    public float maxTime = 90;
    public float currentTime = 0;

    void Start()
    {
        player2 = GetComponent<Animator>();
        GameObject ControladorGameObject = GameObject.FindGameObjectWithTag("GameController");
        Controlador = ControladorGameObject.gameObject.GetComponent<Controlador>();
        player = GetComponent<Transform>();

        m_Source = GetComponent<AudioSource>();

        
        slot = ButtonController.slot2;
        renderHolder = GameObject.Find("PowerUp Holder2").GetComponent<SpriteRenderer>();
        renderHolder.sprite = holderS;

        rendererSprite = GameObject.Find("PowerUp2").GetComponent<SpriteRenderer>();
        rendererSprite.sprite = null;


        StartCoroutine(StunDebuff());
    }

    // Update is called once per frame
    void Update()
    {
        if (CanMove == false)
        {
            StartCoroutine(StunDebuff());
        }

        if (IsSlow == true&&vel>=0.05f)
        {
            StartCoroutine(SlowDebuff(slow));
        }

        if (MoreSpeed == true)
        {
            StartCoroutine(SpeedBuff(velocidad));
        }

        if (Size == true)
        {
            StartCoroutine(SizeBuff(grande));
        }


        if (Controlador.HaveTime==true&&CanMove==true) //Mientras haya tiempo el jugador se podra mover
        {
            if(!PauseMenu.gameIsPaused)
            {
                if(PointToPower>=PointToHavePower)
                {
                    powerUpbar.SetActive(true);
                }
                else
                {
                    powerUpbar.SetActive(false);
                }

                if (PointToPower >= PointToHavePower && Sinput.GetButtonDown("PowerUp", slot))
                {
                    int Lucky;
                    PointToPower = PointToPower - PointToHavePower;
                    Lucky = Random.Range(1, 10);
                    currentTime = 0;
                    Debug.Log(PointToPower);

                    if (Controlador.ScoreP2 <= Controlador.ScoreP1)
                    {
                        if (Lucky <= 6)
                        {
                            Debug.Log("Velocidad");
                            MoreSpeed = true;
                        }
                        else
                        {
                            Debug.Log("ENORME");
                            Size = true;
                        }
                    }
                    else if (Controlador.ScoreP2 >= Controlador.ScoreP1)
                    {
                        if (Lucky <= 7)
                        {
                            Debug.Log("ENORME");
                            Size = true;
                        }
                        else if (Lucky>7)
                        {
                            Debug.Log("Velocidad");
                            MoreSpeed = true;
                        }
                    }
                }

                if (Sinput.GetButton("Up", slot))
                {
                                       
                    player.position = new Vector2(player.position.x, player.position.y + vel);
                    player2.SetTrigger("Up");
                    Direction = 2;
                }
                else if (Sinput.GetButton("Left", slot))
                {
                    
                    player.position = new Vector2(player.position.x - vel, player.position.y);
                    player2.SetTrigger("Left");
                    Direction = 1;
                }
                else if (Sinput.GetButton("Right", slot))
                {
                   
                    player.position = new Vector2(player.position.x + vel, player.position.y);
                    player2.SetTrigger("Right");
                    Direction = 3;
                }
                else if (Sinput.GetButton("Down", slot))
                {
                    
                    player.position = new Vector2(player.position.x, player.position.y - vel);
                    player2.SetTrigger("Down");
                    Direction = 0;
                }
                else
                {
                    player2.SetTrigger("Trigger");
                    Direction = 1;
                }
                BarkCollider.rotation = Quaternion.Euler(0, 0, Directions[Direction]);
            }
            
        }
        else
        {
            player2.SetTrigger("Trigger");
        }
       

    }

    void DecreaseTime()
    {
        currentTime += 1f;
        float calculateTime = currentTime / maxTime;
        SetTime(calculateTime);
    }

    //Aqui va desapareciendo el fillamount conforme al tiempo
    void SetTime(float myTime)
    {
        bar.fillAmount = myTime;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Bone")
        {
            CameraShake.ShakingCam = true;
            CameraShake.CaseShakingCam = 1;
            CanMove = false;
            Destroy(collision.gameObject);
            m_Source.clip = boneDamage;
            m_Source.volume = 0.8f;
            m_Source.Play();
        }
    }

    IEnumerator StunDebuff()
    {
        if (CanMove == false)
        {
            rendererSprite.sprite = stunS;
            yield return new WaitForSeconds(StunDuration);
            CanMove = true;
            rendererSprite.sprite = null;
        }
    }

    IEnumerator SlowDebuff(AudioClip clip)
    {
        if(IsSlow==true)
        {
            IsSlow = false;
            rendererSprite.sprite = slowS;
            m_Source.clip = clip;
            m_Source.volume = 0.8f;
            m_Source.Play();
            vel = vel - DebuffVel;
            yield return new WaitForSeconds(SlowDuration);           
            vel = vel + DebuffVel;
            rendererSprite.sprite = null;
        }
    }

    IEnumerator SpeedBuff(AudioClip clip)
    {
        if (MoreSpeed == true)
        {
            MoreSpeed = false;
            rendererSprite.sprite = speedS;
            m_Source.clip = clip;
            m_Source.volume = 0.15f;
            m_Source.Play();
            vel = vel + BuffVel;
            yield return new WaitForSeconds(SpeedBuffDuration);
            vel = vel - BuffVel;
            rendererSprite.sprite = null;
        }
    }

    IEnumerator SizeBuff(AudioClip clip)
    {
        if (Size == true)
        {
            Size = false;
            rendererSprite.sprite = grandeS;
            m_Source.clip = clip;
            m_Source.volume = 1;
            m_Source.Play();
            player.transform.localScale = new Vector2(player.transform.localScale.x - StatsSize, player.transform.localScale.y + StatsSize);
            yield return new WaitForSeconds(SizeDuration);
            player.transform.localScale = new Vector2(player.transform.localScale.x + StatsSize, player.transform.localScale.y - StatsSize);
            rendererSprite.sprite = null;
        }
    }

    public void PowerUpCharger(int points)
    {
        PointToPower = PointToPower + points;
        //Debug.Log(PointToPower);
        Invoke("DecreaseTime", 1f);
    }
}
