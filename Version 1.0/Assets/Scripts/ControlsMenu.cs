﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ControlsMenu : MonoBehaviour
{
    [SerializeField]
    public Text gamerTag1;
    public Text gamerTag2;

    public Text Escape;

    SinputSystems.InputDeviceSlot p1, p2;
  public void nextLoad()
    {
        SceneManager.LoadScene(1);

    }


    public void Start()
    {
        p1 = ButtonController.slot1;
        p2 = ButtonController.slot2;

    }
    public void Update()
    {
        if(gamerTag1.text!="" && gamerTag2.text!="")
        {
            Escape.gameObject.SetActive(true);
            if (Sinput.GetButton("Submit", p1) || Sinput.GetButton("Submit", p2))
            {
                SaveTags();
                nextLoad();
            }
        }
        else
        {
            Escape.gameObject.SetActive(false);
        }
    
    }

    void SaveTags()
    {
        PlayerPrefs.SetString("Tag1", gamerTag1.text);
        PlayerPrefs.SetString("Tag2", gamerTag2.text);
    }
}
